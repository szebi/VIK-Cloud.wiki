* Update kikapcsolása: No Virus Thans Stop Windows Update
* Induláskor nagyon tekeri a diszket (https://www.youtube.com/watch?v=Enh3gPc3HVs) 
  * **Windows search** service kikapcsolása: Taskmanager -> services 
  * **Superfetch/SysMain** service kikapcsolása: Taskmanager -> services
  * **Defrag** kikapcsolása: Task Scheduler -> defrag (ez mehet a disk -> optimize felületről is)
  * YourPhone uninstall: 
    ```Get-AppxPackage Microsoft.YourPhone -AllUsers | Remove-AppxPackage```
* shutdown kérésre ne hápogjon, hogy valami fut: (https://www.serverbrain.org/2003-guide/using-the-registry-to-manage-shutdown-event-tracker.html)
  * ```reg add HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Reliability /v ShutdownReasonUI  /t REG_DWORD /d 0 /f```
* KMS proxy beállítása és aktiválása az OS-hez:
```
slmgr.vbs /skms poci.cloud.bme.hu
slmgr.vbs /ato
``` 
* KMS proxy beállítása és aktiválása az Office csomaghoz: 
```
cscript ospp.vbs /sethst:poci.cloud.bme.hu
cscript ospp.vbs /setprt:1688
cscript ospp.vbs /act
``` 
* Matlab licence proxy beállítása:
  * $Matlabdir$/licenses/network.lic fájlban az IP-t át kell írni ```poci.cloud.bme.hu```-ra.
* PowerShell history törlés: ```del (Get-PSReadlineOption).HistorySavePath```